package kukisesi

import (
	//"fmt"
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/birowo/base64/lib"
	"lukechampine.com/frand"
)

const (
	setKuki     = "SET-COOKIE"
	kuki        = "COOKIE"
	ssnNm       = "ssnid="
	ssnNmLen    = len(ssnNm)
	httpOnly    = "; HttpOnly"
	httpOnlyLen = len(httpOnly)
	ssnIdLen    = 32
	ssnExp      = "ssnid=; expires=Thu, 01 Jan 1970 00:00:01 GMT"
	ssnExpLen   = len(ssnExp) - ssnNmLen
	expires     = "; expires="
	expiresLen  = len(expires)
	oneDay      = 1
)

func GetId(r *http.Request) (id string) {
	ssnKuki := r.Header.Get(kuki)
	ssnKukiLen := len(ssnKuki)
	if ssnKukiLen > ssnNmLen {
		ssnIdPos := ssnNmLen
		for ssnIdPos < ssnKukiLen && ssnKuki[ssnIdPos-ssnNmLen:ssnIdPos] != ssnNm {
			ssnIdPos++
		}
		if (ssnIdPos + ssnIdLen) <= ssnKukiLen {
			id = ssnKuki[ssnIdPos : ssnIdPos+ssnIdLen]
			return
		}
	}
	return
}
func DelId(w http.ResponseWriter) {
	w.Header().Set(setKuki, ssnExp) //remove session id
}

type RdsPool struct {
	redis.Pool
}

func (p *RdsPool) NewSession(w http.ResponseWriter, val []byte, exp time.Time) {
	ssnKuki := make([]byte, ssnNmLen+ssnIdLen+ssnExpLen+httpOnlyLen)
	copy(ssnKuki, ssnNm)                                                       //ssnid=
	frand.Read(ssnKuki[ssnNmLen+8 : ssnNmLen+ssnIdLen])                        //generate raw id
	b64.FromByteSlc(ssnKuki[ssnNmLen:], ssnKuki[ssnNmLen+8:ssnNmLen+ssnIdLen]) //session id
	if (exp == time.Time{}) {
		copy(ssnKuki[ssnNmLen+ssnIdLen:], httpOnly)                              //; HttpOnly
		w.Header().Set(setKuki, string(ssnKuki[:ssnNmLen+ssnIdLen+httpOnlyLen])) //set session kuki without expires
		exp = time.Now().AddDate(0, 0, oneDay)
	} else {
		copy(ssnKuki[ssnNmLen+ssnIdLen:], expires)                                //; expires=
		copy(ssnKuki[ssnNmLen+ssnIdLen+expiresLen:], exp.Format(http.TimeFormat)) //expires value
		copy(ssnKuki[ssnNmLen+ssnIdLen+ssnExpLen:], httpOnly)                     //; HttpOnly
		w.Header().Set(setKuki, string(ssnKuki))                                  //set session kuki
	}
	conn := p.Pool.Get()
	defer conn.Close()
	conn.Send("MULTI")
	conn.Send("SET", ssnKuki[ssnNmLen:ssnNmLen+ssnIdLen], val)
	conn.Send("EXPIREAT", ssnKuki[ssnNmLen:ssnNmLen+ssnIdLen], exp.Unix())
	_, err := conn.Do("EXEC")
	if err != nil {
		panic(err.Error() + " redis: NewSession  " + string(val))
	}
}
func (p *RdsPool) Del(key string) (err error) {
	conn := p.Pool.Get()
	defer conn.Close()
	_, err = conn.Do("DEL", key)
	return
}
func (p *RdsPool) Set(key string, val []byte) (err error) {
	conn := p.Pool.Get()
	defer conn.Close()
	_, err = conn.Do("SET", key, val)
	return
}
func (p *RdsPool) SetRange(key string, offset int, val []byte) (err error) {
	conn := p.Pool.Get()
	defer conn.Close()
	_, err = conn.Do("SETRANGE", key, offset, val)
	return
}
func (p *RdsPool) Get(key string) ([]byte, error) {
	conn := p.Pool.Get()
	defer conn.Close()
	_val, err := conn.Do("GET", key)
	return _val.([]byte), err
}
func (p *RdsPool) GetRange(key string, bgn, end int) ([]byte, error) {
	conn := p.Pool.Get()
	defer conn.Close()
	_val, err := conn.Do("GETRANGE", key, bgn, end)
	return _val.([]byte), err
}