package main

import (
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/birowo/sessionredis/lib"
)

func root(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Write([]byte(`
<form method="POST" action="/NewSession">
	new kukisesi value: <input name="value"><br>
	expires: <input type="radio" name="expires" value="1"> 1 year <input type="radio" name="expires" value="0"> kukisesi<br>
	<input type="submit">
</form>
		`))
}

type Hndlr func(http.ResponseWriter, *http.Request)

func newSession(rdsPool *kukisesi.RdsPool) Hndlr {
	return func(w http.ResponseWriter, r *http.Request) {
		val := r.PostFormValue("value")
		exp := r.PostFormValue("expires")
		if val == "" || exp == "" {
			http.Redirect(w, r, "http://localhost:8080", http.StatusFound)
			return
		}
		if exp == "1" {
			rdsPool.NewSession(w, []byte(val), time.Now().AddDate(1, 0, 0))
		} else {
			rdsPool.NewSession(w, []byte(val), time.Time{})
		}
		http.Redirect(w, r, "http://localhost:8080/GetSession", http.StatusFound)
	}
}
func getSession(rdsPool *kukisesi.RdsPool) Hndlr {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		v, _ := rdsPool.Get(kukisesi.GetId(r))
		if v != nil {
			w.Write([]byte("kukisesi value: "))
			w.Write(v)
			w.Write([]byte("<br>"))
		}
		w.Write([]byte(`
<form method="POST" action="/UpdateSesionValue">
	update kukisesi value: <input name="value"><br>
	<input type="submit">
</form>
- OR -<br>
<a href="http://localhost:8080/DeleteSession">Delete Session</a>
		`))
	}
}
func delSession(rdsPool *kukisesi.RdsPool) Hndlr {
	return func(w http.ResponseWriter, r *http.Request) {
		rdsPool.Del(kukisesi.GetId(r))
		kukisesi.DelId(w)
		http.Redirect(w, r, "http://localhost:8080", http.StatusFound)
	}
}
func updSsnVal(rdsPool *kukisesi.RdsPool) Hndlr {
	return func(w http.ResponseWriter, r *http.Request) {
		val := r.PostFormValue("value")
		if val != "" {
			rdsPool.Set(kukisesi.GetId(r), []byte(val))
		}
		http.Redirect(w, r, "http://localhost:8080/GetSession", http.StatusFound)
	}
}
func main() {
	rdsPool := &kukisesi.RdsPool{
		redis.Pool{
			Dial: func() (redis.Conn, error) {
				return redis.Dial("tcp", "localhost:7481")
			},
		},
	}
	http.HandleFunc("/", root)
	http.HandleFunc("/NewSession", newSession(rdsPool))
	http.HandleFunc("/GetSession", getSession(rdsPool))
	http.HandleFunc("/DeleteSession", delSession(rdsPool))
	http.HandleFunc("/UpdateSesionValue", updSsnVal(rdsPool))
	http.ListenAndServe(":8080", nil)
}